﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class TipoDocumento
    {
        public int TipoDocumentoId { set; get; }
        public string Descripcion { set; get; }
        public string Convencion { set; get; }
    }
}
