﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Rol
    {
        public Rol()
        {
            this.Permiso = new HashSet<Permiso>();
        }
        public int RolId { set; get; }
        public string Descripcion { set; get; }
        public string Nombre { set; get; }

        public virtual ICollection<Permiso> Permiso { set; get; }
    }
}
