﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Menu
    {
        public int MenuId { set; get; }
        public int MenuFatherId { set; get; }
        public string url { set; get; }
        public bool isActive { set; get; }
        public virtual Rol Rol {set;get;}
    }
}
