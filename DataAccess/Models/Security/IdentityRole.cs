﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.Security
{
    public class IdentityRole : IRole<string>
    {

        /// <summary>Navigation property for users in the role</summary>
        public virtual ICollection<IdentityUserRole> Users { get; private set; }

        /// <summary>Navigation property for users in the role</summary>
        //public virtual ICollection<RolePermission> Permissions { get; private set; }

        ///// <summary>Navigation property for users in the role</summary>
        //public virtual ICollection<RoleNotPossibleCombination> InitialRole { get; private set; }

        ///// <summary>Navigation property for users in the role</summary>
        //public virtual ICollection<RoleNotPossibleCombination> RoleNotPossibleCombination { get; private set; }

        //public virtual ICollection<SurveyRoles> Surveys { get; private set; }

        /// <summary>Role id</summary>
        public string Id { get; set; }

        /// <summary>Role name</summary>
        public string Name { get; set; }

        /// <summary>
        ///     Constructor
        /// </summary>
        public IdentityRole()
        {

            Id = Guid.NewGuid().ToString();
            Users = (ICollection<IdentityUserRole>)new List<IdentityUserRole>();
            //Permissions = (ICollection<RolePermission>)new List<RolePermission>();
            //Surveys = (ICollection<SurveyRoles>)new List<SurveyRoles>();
        }

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="roleName"></param>
        public IdentityRole(string roleName) : this()
        {
            Name = roleName;
        }
    }
}
