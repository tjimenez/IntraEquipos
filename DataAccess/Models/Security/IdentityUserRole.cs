﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.Security
{
    public class IdentityUserRole
    {
        
        /// <summary>UserId for the user that is in the role</summary>
        public virtual string UserId { get; set; }

        /// <summary>RoleId for the role</summary>
        public virtual string RoleId { get; set; }

        /// <summary>RoleId for the role</summary>
        public virtual bool IsInitialRole { get; set; }

    }
}
