﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.Security
{
    public class IdentityUserClaim
    {
        /// <summary>
        ///     Claim type
        /// </summary>
        public virtual string ClaimType
        {
            get;
            set;
        }

        /// <summary>
        ///     Claim value
        /// </summary>
        public virtual string ClaimValue
        {
            get;
            set;
        }

        /// <summary>
        ///     Primary key
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

        /// <summary>
        ///     User Id for the user who owns this login
        /// </summary>
        public virtual string UserId
        {
            get;
            set;
        }

        public IdentityUserClaim()
        {
        }
    }
}
