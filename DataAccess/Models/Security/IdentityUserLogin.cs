﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.Security
{
    public class IdentityUserLogin
    {
        public virtual string LoginProvider
        {
            get;
            set;
        }
        /// <summary>
        ///     Primary key
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }
        /// <summary>
        ///     Key representing the login for the provider
        /// </summary>
        public virtual string ProviderKey
        {
            get;
            set;
        }

        /// <summary>
        ///     User Id for the user who owns this login
        /// </summary>
        public virtual string UserId
        {
            get;
            set;
        }

        public IdentityUserLogin()
        {
        }
    }
}
