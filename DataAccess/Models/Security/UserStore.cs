﻿
using Exsis.VirtualClinic.Core.Domain.Resources;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess.Models.Security
{
    public class UserStore :
        IUserLoginStore<Persona, string>,
        IUserClaimStore<Persona, string>,
        IUserRoleStore<Persona, string>,
        IUserPasswordStore<Persona, string>,
        IUserSecurityStampStore<Persona, string>,
        IQueryableUserStore<Persona, string>,
        IUserEmailStore<Persona, string>,
        IUserPhoneNumberStore<Persona, string>,
        IUserTwoFactorStore<Persona, string>,
        IUserLockoutStore<Persona, string>,
        IUserStore<Persona, string>,
        IUserStore<Persona>,
        IDisposable
    {


        public UserStore(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            this.Context = context;
            this.AutoSaveChanges = true;
            this._userStore = new EntityStore<Persona>(context);
            this._roleStore = new EntityStore<IdentityRole>(context);
            this._logins = this.Context.Set<IdentityUserLogin>();
            this._userClaims = this.Context.Set<IdentityUserClaim>();
            this._userRoles = this.Context.Set<IdentityUserRole>();
        }
        private readonly IDbSet<IdentityUserLogin> _logins;
        private readonly EntityStore<IdentityRole> _roleStore;
        private readonly IDbSet<IdentityUserClaim> _userClaims;
        private readonly IDbSet<IdentityUserRole> _userRoles;

        private bool _disposed;
        public IQueryable<Persona> Users {
            get
            {
                return this._userStore.EntitySet;
            }
        }

        public DbContext Context
        {
            get;
            private set;
        }
        public bool AutoSaveChanges
        {
            get;
            set;
        }
        public bool DisposeContext
        {
            get;
            set;
        }

        private EntityStore<Persona> _userStore;

        public Task AddClaimAsync(Persona user, Claim claim)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            IDbSet<IdentityUserClaim> tUserClaims = this._userClaims;
            IdentityUserClaim id = Activator.CreateInstance<IdentityUserClaim>();
            id.UserId = user.Id;
            id.ClaimType = claim.Type;
            id.ClaimValue = claim.Value;
            tUserClaims.Add(id);
            return Task.FromResult<int>(0);
        }

        public Task AddLoginAsync(Persona user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public virtual async Task AddToRoleAsync(Persona user, string roleName)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, "roleName");
            }
            DbSet<IdentityRole> dbEntitySet = this._roleStore.DbEntitySet;
            TaskExtensions.CultureAwaiter<IdentityRole> cultureAwaiter = QueryableExtensions.SingleOrDefaultAsync<IdentityRole>(dbEntitySet, (IdentityRole r) => r.Name.ToUpper() == roleName.ToUpper()).WithCurrentCulture<IdentityRole>();
            IdentityRole tRole = await cultureAwaiter;
            if (tRole == null)
            {
                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                string roleNotFound = IdentityResources.RoleNotFound;
                object[] objArray = new object[] { roleName };
                throw new InvalidOperationException(string.Format(currentCulture, roleNotFound, objArray));
            }
            IdentityUserRole id = Activator.CreateInstance<IdentityUserRole>();
            id.UserId = user.Id;
            id.RoleId = tRole.Id;
            this._userRoles.Add(id);
        }

        public virtual async Task CreateAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            this._userStore.Create(user);
            await this.SaveChanges().WithCurrentCulture();
        }
        private async Task SaveChanges()
        {
            if (this.AutoSaveChanges)
            {
                TaskExtensions.CultureAwaiter<int> cultureAwaiter = this.Context.SaveChangesAsync().WithCurrentCulture<int>();
                await cultureAwaiter;
            }
        }
        public Task DeleteAsync(Persona user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this); 
        }
        protected virtual void Dispose(bool disposing)
        {
            if (this.DisposeContext && disposing && this.Context != null)
            {
                this.Context.Dispose();
            }
            this._disposed = true;
            this.Context = null;
            this._userStore = null;
        }
        public Task<Persona> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<Persona> FindByEmailAsync(string email)
        {
            this.ThrowIfDisposed();
            return this.GetUserAggregateAsync((Persona u) => u.UserName.ToUpper() == email.ToUpper());
        }

        public Task<Persona> FindByIdAsync(string userId)
        {
            this.ThrowIfDisposed();
            return this.GetUserAggregateAsync((Persona u) => u.Id.Equals(userId));
        }

        public Task<Persona> FindByNameAsync(string userName)
        {
            this.ThrowIfDisposed();
            return this.GetUserAggregateAsync((Persona u) => u.UserName.ToUpper() == userName.ToUpper());
        }
        private async Task EnsureClaimsLoaded(Persona user)
        {
            if (!this.AreClaimsLoaded(user))
            {
                string id = user.Id;
                IDbSet<IdentityUserClaim> tUserClaims = this._userClaims;
                TaskExtensions.CultureAwaiter cultureAwaiter = (
                    from uc in tUserClaims
                    where uc.UserId.Equals(id)
                    select uc).LoadAsync().WithCurrentCulture();
                await cultureAwaiter;
                DbEntityEntry<Persona> dbEntityEntry = this.Context.Entry<Persona>(user);
                dbEntityEntry.Collection<IdentityUserClaim>((Persona u) => u.Claims).IsLoaded = true;
            }
        }
        private async Task EnsureLoginsLoaded(Persona user)
        {
            if (!this.AreLoginsLoaded(user))
            {
                string id = user.Id;
                IDbSet<IdentityUserLogin> tUserLogins = this._logins;
                TaskExtensions.CultureAwaiter cultureAwaiter = (
                    from uc in tUserLogins
                    where uc.UserId.Equals(id)
                    select uc).LoadAsync().WithCurrentCulture();
                await cultureAwaiter;
                DbEntityEntry<Persona> dbEntityEntry = this.Context.Entry<Persona>(user);
                dbEntityEntry.Collection<IdentityUserLogin>((Persona u) => u.Logins).IsLoaded = true;
            }
        }

        private bool AreLoginsLoaded(Persona user)
        {
            return this.Context.Entry<Persona>(user).Collection<IdentityUserLogin>((Persona u) => u.Logins).IsLoaded;
        }

        private bool AreClaimsLoaded(Persona user)
        {
            return this.Context.Entry<Persona>(user).Collection<IdentityUserClaim>((Persona u) => u.Claims).IsLoaded;
        }
        private async Task EnsureRolesLoaded(Persona user)
        {
            DbEntityEntry<Persona> dbEntityEntry = this.Context.Entry<Persona>(user);
            if (!dbEntityEntry.Collection<IdentityUserRole>((Persona u) => u.Roles).IsLoaded)
            {
                string id = user.Id;
                IDbSet<IdentityUserRole> tUserRoles = this._userRoles;
                TaskExtensions.CultureAwaiter cultureAwaiter = (
                    from uc in tUserRoles
                    where uc.UserId.Equals(id)
                    select uc).LoadAsync().WithCurrentCulture();
                await cultureAwaiter;
                DbEntityEntry<Persona> dbEntityEntry1 = this.Context.Entry<Persona>(user);
                dbEntityEntry1.Collection<IdentityUserRole>((Persona u) => u.Roles).IsLoaded = true;
            }
        }
        protected virtual async Task<Persona> GetUserAggregateAsync(Expression<Func<Persona, bool>> filter)
        {
            string tKey;
            Persona tUser;
            if (!UserStore.FindByIdFilterParser.TryMatchAndGetId(filter, out tKey))
            {
                TaskExtensions.CultureAwaiter<Persona> cultureAwaiter = this.Users.FirstOrDefaultAsync<Persona>(filter).WithCurrentCulture<Persona>();
                tUser = await cultureAwaiter;
            }
            else
            {
                TaskExtensions.CultureAwaiter<Persona> cultureAwaiter1 = this._userStore.GetByIdAsync(tKey).WithCurrentCulture<Persona>();
                tUser = await cultureAwaiter1;
            }
            if (tUser != null)
            {
                await this.EnsureClaimsLoaded(tUser).WithCurrentCulture();
                await this.EnsureLoginsLoaded(tUser).WithCurrentCulture();
                await this.EnsureRolesLoaded(tUser).WithCurrentCulture();
            }
            return tUser;
        }
        public Task<int> GetAccessFailedCountAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<int>(user.AccessFailedCount);
        }

        public virtual async Task<IList<Claim>> GetClaimsAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.EnsureClaimsLoaded(user).WithCurrentCulture();
            ICollection<IdentityUserClaim> claims = user.Claims;
            IList<Claim> list = (
                from c in claims
                select new Claim(c.ClaimType, c.ClaimValue)).ToList<Claim>();
            return list;
        }

        public Task<string> GetEmailAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<string>(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<bool>(user.EmailConfirmed);
        }

        public Task<bool> GetLockoutEnabledAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<bool>(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(Persona user)
        {
            DateTimeOffset dateTimeOffset;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (user.LockoutEndDateUtc.HasValue)
            {
                DateTime? lockoutEndDateUtc = user.LockoutEndDateUtc;
                dateTimeOffset = new DateTimeOffset(DateTime.SpecifyKind(lockoutEndDateUtc.Value, DateTimeKind.Utc));
            }
            else
            {
                dateTimeOffset = new DateTimeOffset();
            }
            return Task.FromResult<DateTimeOffset>(dateTimeOffset);
        }

        public virtual async Task<IList<UserLoginInfo>> GetLoginsAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            await this.EnsureLoginsLoaded(user).WithCurrentCulture();
            ICollection<IdentityUserLogin> logins = user.Logins;
            IList<UserLoginInfo> list = (
                from l in logins
                select new UserLoginInfo(l.LoginProvider, l.ProviderKey)).ToList<UserLoginInfo>();
            return list;
        }

        public Task<string> GetPasswordHashAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<string>(user.PasswordHash);
        }

        public Task<string> GetPhoneNumberAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<string>(user.PhoneNumber);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<bool>(user.PhoneNumberConfirmed);
        }

        public virtual async Task<IList<string>> GetRolesAsync(Persona user)
        {
            {
                this.ThrowIfDisposed();
                if (user == null)
                {
                    throw new ArgumentNullException("user");
                }
                string id = user.Id;
                IDbSet<IdentityUserRole> tUserRoles = this._userRoles;
                IQueryable<IdentityUserRole> tUserRoles1 =
                    from userRole in tUserRoles
                    where userRole.UserId.Equals(id)
                    select userRole;
                DbSet<IdentityRole> dbEntitySet = this._roleStore.DbEntitySet;
                Expression<Func<IdentityUserRole, string>> roleId = (IdentityUserRole userRole) => userRole.RoleId;
                Expression<Func<IdentityRole, string>> expression = (IdentityRole role) => role.Id;
                IQueryable<string> strs = tUserRoles1.Join<IdentityUserRole, IdentityRole, string, string>(dbEntitySet, roleId, expression, (IdentityUserRole userRole, IdentityRole role) => role.Name);
                return await strs.ToListAsync<string>().WithCurrentCulture<List<string>>();
            }
        }
        public Task<string> GetSecurityStampAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<string>(user.SecurityStamp);
        }

        public Task<bool> GetTwoFactorEnabledAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            return Task.FromResult<bool>(user.TwoFactorEnabled);
        }

        public Task<bool> HasPasswordAsync(Persona user)
        {
            return Task.FromResult<bool>(user.PasswordHash != null);
        }

        public Task<int> IncrementAccessFailedCountAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.AccessFailedCount = user.AccessFailedCount + 1;
            return Task.FromResult<int>(user.AccessFailedCount);
        }

        public Task<bool> IsInRoleAsync(Persona user, string roleName)
        {
            throw new NotImplementedException();
        }

        public async Task RemoveClaimAsync(Persona user, Claim claim)
        {
            IEnumerable<IdentityUserClaim> list;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (claim == null)
            {
                throw new ArgumentNullException("claim");
            }
            string value = claim.Value;
            string type = claim.Type;
            if (!this.AreClaimsLoaded(user))
            {
                string id = user.Id;
                IDbSet<IdentityUserClaim> tUserClaims = this._userClaims;
                TaskExtensions.CultureAwaiter<List<IdentityUserClaim>> cultureAwaiter = (
                    from uc in tUserClaims
                    where (uc.ClaimValue == value) && (uc.ClaimType == type) && uc.UserId.Equals(id)
                    select uc).ToListAsync<IdentityUserClaim>().WithCurrentCulture<List<IdentityUserClaim>>();
                list = await cultureAwaiter;
            }
            else
            {
                ICollection<IdentityUserClaim> claims = user.Claims;
                list = claims.Where<IdentityUserClaim>((IdentityUserClaim uc) =>
                {
                    if (uc.ClaimValue != value)
                    {
                        return false;
                    }
                    return uc.ClaimType == type;
                }).ToList<IdentityUserClaim>();
            }
            foreach (IdentityUserClaim tUserClaim in list)
            {
                this._userClaims.Remove(tUserClaim);
            }
        }

        public async Task RemoveFromRoleAsync(Persona user, string roleName)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException(IdentityResources.ValueCannotBeNullOrEmpty, "roleName");
            }
            DbSet<IdentityRole> dbEntitySet = this._roleStore.DbEntitySet;
            TaskExtensions.CultureAwaiter<IdentityRole> cultureAwaiter = QueryableExtensions.SingleOrDefaultAsync<IdentityRole>(dbEntitySet, (IdentityRole r) => r.Name.ToUpper() == roleName.ToUpper()).WithCurrentCulture<IdentityRole>();
            IdentityRole tRole = await cultureAwaiter;
            if (tRole != null)
            {
                string id = tRole.Id;
                string tKey = user.Id;
                IDbSet<IdentityUserRole> tUserRoles = this._userRoles;
                TaskExtensions.CultureAwaiter<IdentityUserRole> cultureAwaiter1 = tUserRoles.FirstOrDefaultAsync<IdentityUserRole>((IdentityUserRole r) => id.Equals(r.RoleId) && r.UserId.Equals(tKey)).WithCurrentCulture<IdentityUserRole>();
                IdentityUserRole tUserRole = await cultureAwaiter1;
                if (tUserRole != null)
                {
                    this._userRoles.Remove(tUserRole);
                }
            }
        }

        public async Task RemoveLoginAsync(Persona user, UserLoginInfo login)
        {
            IdentityUserLogin tUserLogin;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            string loginProvider = login.LoginProvider;
            string providerKey = login.ProviderKey;
            if (!this.AreLoginsLoaded(user))
            {
                string id = user.Id;
                IDbSet<IdentityUserLogin> tUserLogins = this._logins;
                TaskExtensions.CultureAwaiter<IdentityUserLogin> cultureAwaiter = QueryableExtensions.SingleOrDefaultAsync<IdentityUserLogin>(tUserLogins, (IdentityUserLogin ul) => (ul.LoginProvider == loginProvider) && (ul.ProviderKey == providerKey) && ul.UserId.Equals(id)).WithCurrentCulture<IdentityUserLogin>();
                tUserLogin = await cultureAwaiter;
            }
            else
            {
                ICollection<IdentityUserLogin> logins = user.Logins;
                tUserLogin = logins.SingleOrDefault<IdentityUserLogin>((IdentityUserLogin ul) =>
                {
                    if (ul.LoginProvider != loginProvider)
                    {
                        return false;
                    }
                    return ul.ProviderKey == providerKey;
                });
            }
            if (tUserLogin != null)
            {
                this._logins.Remove(tUserLogin);
            }
        }

        public Task ResetAccessFailedCountAsync(Persona user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.AccessFailedCount = 0;
            return Task.FromResult<int>(0);
        }

        public Task SetEmailAsync(Persona user, string email)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.Email = email;
            return Task.FromResult<int>(0);
        }

        public Task SetEmailConfirmedAsync(Persona user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.EmailConfirmed = confirmed;
            return Task.FromResult<int>(0);
        }

        public Task SetLockoutEnabledAsync(Persona user, bool enabled)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.LockoutEnabled = enabled;
            return Task.FromResult<int>(0); ;
        }

        public Task SetLockoutEndDateAsync(Persona user, DateTimeOffset lockoutEnd)
        {
            DateTime? nullable;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (lockoutEnd == DateTimeOffset.MinValue)
            {
                nullable = null;
            }
            else
            {
                nullable = new DateTime?(lockoutEnd.UtcDateTime);
            }
            user.LockoutEndDateUtc = nullable;
            return Task.FromResult<int>(0);
        }

        public Task SetPasswordHashAsync(Persona user, string passwordHash)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PasswordHash = passwordHash;
            return Task.FromResult<int>(0);
        }

        public Task SetPhoneNumberAsync(Persona user, string phoneNumber)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PhoneNumber = phoneNumber;
            return Task.FromResult<int>(0);
        }

        public Task SetPhoneNumberConfirmedAsync(Persona user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult<int>(0);
        }

        public Task SetSecurityStampAsync(Persona user, string stamp)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            user.SecurityStamp = stamp;
            return Task.FromResult<int>(0);
        }

        public Task SetTwoFactorEnabledAsync(Persona user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Persona user)
        {
            throw new NotImplementedException();
        }


        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }
        private static class FindByIdFilterParser
        {
            private readonly static Expression<Func<Persona, bool>> Predicate;

            private readonly static MethodInfo EqualsMethodInfo;

            private readonly static MemberInfo UserIdMemberInfo;

            static FindByIdFilterParser()
            {
                UserStore.FindByIdFilterParser.Predicate = (Persona u) => u.Id.Equals(default(string));
                UserStore.FindByIdFilterParser.EqualsMethodInfo = ((MethodCallExpression)UserStore.FindByIdFilterParser.Predicate.Body).Method;
                UserStore.FindByIdFilterParser.UserIdMemberInfo = ((MemberExpression)((MethodCallExpression)UserStore.FindByIdFilterParser.Predicate.Body).Object).Member;
            }

            internal static bool TryMatchAndGetId(Expression<Func<Persona, bool>> filter, out string id)
            {
                MemberExpression item;
                id = default(string);
                if (filter.Body.NodeType != ExpressionType.Call)
                {
                    return false;
                }
                MethodCallExpression body = (MethodCallExpression)filter.Body;
                if (body.Method != UserStore.FindByIdFilterParser.EqualsMethodInfo)
                {
                    return false;
                }
                if (body.Object == null || body.Object.NodeType != ExpressionType.MemberAccess || ((MemberExpression)body.Object).Member != UserStore.FindByIdFilterParser.UserIdMemberInfo)
                {
                    return false;
                }
                if (body.Arguments.Count != 1)
                {
                    return false;
                }
                if (body.Arguments[0].NodeType != ExpressionType.Convert)
                {
                    if (body.Arguments[0].NodeType != ExpressionType.MemberAccess)
                    {
                        return false;
                    }
                    item = (MemberExpression)body.Arguments[0];
                }
                else
                {
                    UnaryExpression unaryExpression = (UnaryExpression)body.Arguments[0];
                    if (unaryExpression.Operand.NodeType != ExpressionType.MemberAccess)
                    {
                        return false;
                    }
                    item = (MemberExpression)unaryExpression.Operand;
                }
                if (item.Member.MemberType != MemberTypes.Field || item.Expression.NodeType != ExpressionType.Constant)
                {
                    return false;
                }
                FieldInfo member = (FieldInfo)item.Member;
                object value = ((ConstantExpression)item.Expression).Value;
                id = (string)member.GetValue(value);
                return true;
            }
        }
    }
    internal static class TaskExtensions
    {
        public static TaskExtensions.CultureAwaiter<T> WithCurrentCulture<T>(this Task<T> task)
        {
            return new TaskExtensions.CultureAwaiter<T>(task);
        }

        public static TaskExtensions.CultureAwaiter WithCurrentCulture(this Task task)
        {
            return new TaskExtensions.CultureAwaiter(task);
        }

        public struct CultureAwaiter : ICriticalNotifyCompletion, INotifyCompletion
        {
            private readonly Task _task;

            public bool IsCompleted
            {
                get
                {
                    return this._task.IsCompleted;
                }
            }

            public CultureAwaiter(Task task)
            {
                this._task = task;
            }

            public TaskExtensions.CultureAwaiter GetAwaiter()
            {
                return this;
            }

            public void GetResult()
            {
                this._task.GetAwaiter().GetResult();
            }

            public void OnCompleted(Action continuation)
            {
                throw new NotImplementedException();
            }

            public void UnsafeOnCompleted(Action continuation)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                CultureInfo cultureInfo1 = Thread.CurrentThread.CurrentUICulture;
                ConfiguredTaskAwaitable.ConfiguredTaskAwaiter awaiter = this._task.ConfigureAwait(false).GetAwaiter();
                awaiter.UnsafeOnCompleted(() => {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    Thread.CurrentThread.CurrentUICulture = cultureInfo1;
                    try
                    {
                        continuation();
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = currentCulture;
                        Thread.CurrentThread.CurrentUICulture = currentUICulture;
                    }
                });
            }
        }

        public struct CultureAwaiter<T> : ICriticalNotifyCompletion, INotifyCompletion
        {
            private readonly Task<T> _task;

            public bool IsCompleted
            {
                get
                {
                    return this._task.IsCompleted;
                }
            }

            public CultureAwaiter(Task<T> task)
            {
                this._task = task;
            }

            public TaskExtensions.CultureAwaiter<T> GetAwaiter()
            {
                return this;
            }

            public T GetResult()
            {
                return this._task.GetAwaiter().GetResult();
            }

            public void OnCompleted(Action continuation)
            {
                throw new NotImplementedException();
            }

            public void UnsafeOnCompleted(Action continuation)
            {
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                CultureInfo cultureInfo1 = Thread.CurrentThread.CurrentUICulture;
                ConfiguredTaskAwaitable<T>.ConfiguredTaskAwaiter awaiter = this._task.ConfigureAwait(false).GetAwaiter();
                awaiter.UnsafeOnCompleted(() => {
                    CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                    CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    Thread.CurrentThread.CurrentUICulture = cultureInfo1;
                    try
                    {
                        continuation();
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = currentCulture;
                        Thread.CurrentThread.CurrentUICulture = currentUICulture;
                    }
                });
            }
        }

    }
   
}
