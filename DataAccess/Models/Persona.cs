﻿using DataAccess.Models.Security;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Persona : IUser, IUser<string>
    {

        public Persona()
        {
            this.Id = Guid.NewGuid().ToString();
            this._claims = new List<IdentityUserClaim>();
            this._roles = new List<IdentityUserRole>();
            this._logins = new List<IdentityUserLogin>();
        }
        private ICollection<IdentityUserClaim> _claims;

        private ICollection<IdentityUserLogin> _logins;

        private ICollection<IdentityUserRole> _roles;

        public int PersonaId { set; get; }
        public string Apellido { set; get; }
        public string NumeroDocumento { set; get; }
        public string Nombre { set; get; }
        public int TipoDocumentoId { set; get; }
        public string Telefono { set; get; }

        public virtual int AccessFailedCount { get; set; }

        public virtual TipoDocumento TipoDocumento { set; get; }

        public virtual string Id { set; get; }

        public virtual bool TwoFactorEnabled { get; set; }

        public virtual string PasswordHash { set; get; }

        public virtual string SecurityQuestionAnswer { get; set; }

        public virtual bool PhoneNumberConfirmed { get; set; }

        public virtual int? SecurityQuestionId { get; set; }

        /// <summary>
        ///     A random value that should change whenever a users credentials have changed (password changed, login removed)
        /// </summary>
        public virtual string SecurityStamp { get; set; }

        public virtual string PhoneNumber { get; set; }

        public virtual bool LockoutEnabled { get; set; }

        public virtual DateTime? LockoutEndDateUtc { get; set; }

        public virtual string Email { get; set; }

        public virtual bool EmailConfirmed { get; set; }
        /// <summary>
        /// Email when is registered an user
        /// </summary>
        public virtual string UserName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Persona> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<Persona> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
        public ICollection<IdentityUserClaim> Claims
        {
            get
            {
                return _claims;
            }
            set
            {
                _claims = value;
            }
        }
        public ICollection<IdentityUserLogin> Logins
        {
            get
            {
                return _logins;
            }
            set
            {
                _logins = value;
            }
        }

        public ICollection<IdentityUserRole> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
            }
        }
    }
}
