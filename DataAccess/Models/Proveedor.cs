﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Models
{
    public class Proveedor
    {
        public int ProveedorId { set; get; }
        public string Direccion { set; get; }
        public string Email { set; get; }
        public string PaginaWeb { set; get; }
        public string RazonSocial { set; get; }
        public string Slogan { set; get; }
        public string Telefono { set; get; } 
        
        public virtual ICollection<Producto> Producto { set; get; }
    }
}
