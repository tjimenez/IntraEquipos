﻿using DataAccess.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Permiso
    {
        public Permiso()
        {
            this.Rol = new HashSet<IdentityRole>();
        }
        public int PermisoId { set; get; }

        public int? PermisoPadreId { set; get; }
        public string Nombre { set; get; }
        public string UrlPermiso { set; get; }
        public bool IsActive { set; get; }
        public virtual ICollection<IdentityRole> Rol { set; get; }
    }
}
