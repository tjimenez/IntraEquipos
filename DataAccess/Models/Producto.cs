﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public class Producto
    {
        public int ProductoId { set; get; }
        public decimal? Costo { set; get; }
        public string Descripcion { set; get; }
        public int? ProveedorId { set; get; }
        public string Origen { set; get; }

        public virtual Proveedor Proveedor { set; get; }
    }
}
