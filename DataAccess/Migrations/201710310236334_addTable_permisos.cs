namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTable_permisos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permisoes",
                c => new
                    {
                        PermisoId = c.Int(nullable: false, identity: true),
                        PermisoPadreId = c.Int(),
                        Nombre = c.String(maxLength: 50),
                        UrlPermiso = c.String(maxLength: 100),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PermisoId);
            
            AddColumn("dbo.Roles", "Permiso_PermisoId", c => c.Int());
            CreateIndex("dbo.Roles", "Permiso_PermisoId");
            AddForeignKey("dbo.Roles", "Permiso_PermisoId", "dbo.Permisoes", "PermisoId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Roles", "Permiso_PermisoId", "dbo.Permisoes");
            DropIndex("dbo.Roles", new[] { "Permiso_PermisoId" });
            DropColumn("dbo.Roles", "Permiso_PermisoId");
            DropTable("dbo.Permisoes");
        }
    }
}
