namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        PersonaId = c.Int(nullable: false),
                        Apellido = c.String(nullable: false, maxLength: 50),
                        NumeroDocumento = c.String(nullable: false, maxLength: 20),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        TipoDocumentoId = c.Int(nullable: false),
                        Telefono = c.String(nullable: false, maxLength: 15),
                        AccessFailedCount = c.Int(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        PasswordHash = c.String(maxLength: 200),
                        SecurityQuestionAnswer = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        SecurityQuestionId = c.Int(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(maxLength: 30),
                        LockoutEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TiposDocumento", t => t.TipoDocumentoId)
                .Index(t => t.TipoDocumentoId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.IdentityUserLogins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        IsInitialRole = c.Boolean(nullable: false),
                        IdentityRole_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Personas", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.IdentityRole_Id)
                .Index(t => t.UserId)
                .Index(t => t.IdentityRole_Id);
            
            CreateTable(
                "dbo.TiposDocumento",
                c => new
                    {
                        TipoDocumentoId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(maxLength: 100),
                        Convencion = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.TipoDocumentoId);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        ProductoId = c.Int(nullable: false, identity: true),
                        Costo = c.Decimal(precision: 18, scale: 2),
                        Descripcion = c.String(maxLength: 100),
                        ProveedorId = c.Int(),
                        Origen = c.String(maxLength: 70),
                        Proveedor_ProveedorId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductoId)
                .ForeignKey("dbo.Proveedores", t => t.Proveedor_ProveedorId)
                .ForeignKey("dbo.Proveedores", t => t.ProveedorId)
                .Index(t => t.ProveedorId)
                .Index(t => t.Proveedor_ProveedorId);
            
            CreateTable(
                "dbo.Proveedores",
                c => new
                    {
                        ProveedorId = c.Int(nullable: false, identity: true),
                        Direccion = c.String(maxLength: 40),
                        Email = c.String(nullable: false, maxLength: 50),
                        PaginaWeb = c.String(maxLength: 70),
                        RazonSocial = c.String(nullable: false, maxLength: 40),
                        Slogan = c.String(maxLength: 50),
                        Telefono = c.String(maxLength: 20),
                    })
                .PrimaryKey(t => t.ProveedorId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "IdentityRole_Id", "dbo.Roles");
            DropForeignKey("dbo.Productos", "ProveedorId", "dbo.Proveedores");
            DropForeignKey("dbo.Productos", "Proveedor_ProveedorId", "dbo.Proveedores");
            DropForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Personas");
            DropForeignKey("dbo.IdentityUserLogins", "UserId", "dbo.Personas");
            DropForeignKey("dbo.IdentityUserClaims", "UserId", "dbo.Personas");
            DropIndex("dbo.Productos", new[] { "Proveedor_ProveedorId" });
            DropIndex("dbo.Productos", new[] { "ProveedorId" });
            DropIndex("dbo.UserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.IdentityUserLogins", new[] { "UserId" });
            DropIndex("dbo.IdentityUserClaims", new[] { "UserId" });
            DropIndex("dbo.Personas", "UserNameIndex");
            DropIndex("dbo.Personas", new[] { "TipoDocumentoId" });
            DropTable("dbo.Roles");
            DropTable("dbo.Proveedores");
            DropTable("dbo.Productos");
            DropTable("dbo.TiposDocumento");
            DropTable("dbo.UserRoles");
            DropTable("dbo.IdentityUserLogins");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.Personas");
        }
    }
}
