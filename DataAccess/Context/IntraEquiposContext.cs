﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DataAccess.Models;
using DataAccess.Configurations;
using DataAccess.Models.Security;

namespace DataAccess.Context
{
    public class IntraEquiposContext : DbContext
    {
        public IntraEquiposContext() : base("IntraEquiposContext")
        {

        }
        public DbSet<Persona> Persona { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<TipoDocumento> TipoDocumento { get; set; }
        public DbSet<Proveedor> Proveedor { set; get; }
        public DbSet<Permiso> Permiso { set; get; }
        public virtual IDbSet<IdentityRole> Roles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new PersonaConfiguration());
            modelBuilder.Configurations.Add(new PermisoConfiguration());
            modelBuilder.Configurations.Add(new ProductoConfiguration());
            modelBuilder.Configurations.Add(new ProveedorConfiguration());
            modelBuilder.Configurations.Add(new TipoDocumentoConfiguration());
            modelBuilder.Entity<IdentityUserRole>().HasKey((IdentityUserRole r) => new { UserId = r.UserId, RoleId = r.RoleId }).ToTable("UserRoles");
            modelBuilder.Entity<IdentityRole>().HasKey((IdentityRole r) => new { Id= r.Id }).ToTable("Roles");            //modelBuilder.Entity<RolePermission>().HasKey((RolePermission r) => new { RoleId = r.RoleId, PermissionId = r.PermissionId }).ToTable("RolePermissions").HasRequired<IdentityRole>(q => q.Role).WithMany(s => s.Permissions).HasForeignKey(s => s.RoleId);
            base.OnModelCreating(modelBuilder);

        }


        public static IntraEquiposContext Create()
        {
            return new IntraEquiposContext();
        }
    }
}
