﻿using DataAccess.Models;
using DataAccess.Models.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Configurations
{
    public class PersonaConfiguration : EntityTypeConfiguration<Persona>
    {
        public PersonaConfiguration()
        {
            //HasKey(m => m.Id);  
            ToTable("Personas");
            HasMany<IdentityUserRole>((Persona u) => u.Roles).WithRequired().HasForeignKey<string>((IdentityUserRole ur) => ur.UserId);
            HasMany<IdentityUserClaim>((Persona u) => u.Claims).WithRequired().HasForeignKey<string>((IdentityUserClaim uc) => uc.UserId);
            HasMany<IdentityUserLogin>((Persona u) => u.Logins).WithRequired().HasForeignKey<string>((IdentityUserLogin ul) => ul.UserId);
            StringPropertyConfiguration stringPropertyConfiguration = Property(u => u.UserName).IsRequired().HasMaxLength(new int?(256));
            IndexAttribute indexAttribute = new IndexAttribute("UserNameIndex")
            {
                IsUnique = true
            };
            stringPropertyConfiguration.HasColumnAnnotation("Index", new IndexAnnotation(indexAttribute));
            Property(m => m.Nombre).IsRequired().HasMaxLength(50);
            Property(m => m.Apellido).IsRequired().HasMaxLength(50);
            Property(m => m.NumeroDocumento).IsRequired().HasMaxLength(20);
            Property(m => m.Telefono).IsRequired().HasMaxLength(15);
            Property(m => m.UserName).HasMaxLength(200);
            Property(m => m.PasswordHash).HasMaxLength(200);
            Property(m => m.PhoneNumber).HasMaxLength(30);
            Property(m => m.AccessFailedCount);
            Property(m => m.LockoutEnabled);
            Property(m => m.LockoutEndDateUtc).IsOptional();
            Property(m => m.EmailConfirmed);
            Property(m => m.Email);
          
            HasRequired<TipoDocumento>(m => m.TipoDocumento).WithMany().HasForeignKey(m => m.TipoDocumentoId).WillCascadeOnDelete(false);

        }
    }
}
