﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Configurations
{
    public class PermisoConfiguration: EntityTypeConfiguration<Permiso>
    {
        public PermisoConfiguration() {
            HasKey(m => m.PermisoId);
            Property(m => m.Nombre).HasMaxLength(50);
            Property(m => m.UrlPermiso).HasMaxLength(100);
            Property(m => m.PermisoPadreId).IsOptional();
            Property(m=>m.IsActive).IsRequired();
        }
    }
}
