﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Configurations
{
    public class ProveedorConfiguration : EntityTypeConfiguration<Proveedor>
    {

        public ProveedorConfiguration()
        {
            ToTable("Proveedores");
            HasKey(m => m.ProveedorId);
            Property(m => m.Direccion).IsOptional().HasMaxLength(40);
            Property(m => m.Email).IsRequired().HasMaxLength(50);
            Property(m => m.PaginaWeb).IsOptional().HasMaxLength(70);
            Property(m => m.RazonSocial).IsRequired().HasMaxLength(40);
            Property(m => m.Telefono).IsOptional().HasMaxLength(20);
            Property(m => m.Slogan).IsOptional().HasMaxLength(50);
            
        }
    }
}
