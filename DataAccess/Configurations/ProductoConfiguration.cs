﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Configurations
{
    public class ProductoConfiguration : EntityTypeConfiguration<Producto>
    {
        public ProductoConfiguration()
        {
            ToTable("Productos");
            HasKey(m => m.ProductoId);
            Property(m => m.Origen).IsOptional().HasMaxLength(70);
            Property(m => m.Descripcion).IsOptional().HasMaxLength(100);
            Property(m => m.Costo).IsOptional();
            HasOptional<Proveedor>(m => m.Proveedor).WithMany().HasForeignKey(m => m.ProveedorId);
        }
    }
}
