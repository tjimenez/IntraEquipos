﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Configurations
{
    public class TipoDocumentoConfiguration : EntityTypeConfiguration<TipoDocumento>
    {
        public TipoDocumentoConfiguration()
        {
            ToTable("TiposDocumento");
            HasKey(m => m.TipoDocumentoId);
            Property(m => m.Convencion).HasMaxLength(10);
            Property(m => m.Descripcion).HasMaxLength(100);

        }
    }
}
