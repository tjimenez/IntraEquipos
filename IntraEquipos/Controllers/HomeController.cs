﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IntraEquipos.Controllers
{
    //[ValidateAntiForgeryToken]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var userLogged = new PersonaRepository().Get(m => m.UserName == User.Identity.Name);
            Session["User"] = userLogged;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Resgistrar()
        {
            
            return View();
        }

        public ActionResult Menu()
        {
            PermisoRepository _PermisoRepository = new PermisoRepository();
            var model = _PermisoRepository.Entity.ToList();
            return View(model);
        }
        public ActionResult CreateMenu()
        {

            return View();
        }
    }
}