﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IntraEquipos.Startup))]
namespace IntraEquipos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
