﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repository;
namespace IntraEquipos.Areas.Producto.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto/Producto
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// MODEL
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CreateProduct(DataAccess.Models.Producto model)
        {
            ProductoRepository _productoRrepository = new ProductoRepository();
            _productoRrepository.Add(model);
            return View();
        }
    }
}