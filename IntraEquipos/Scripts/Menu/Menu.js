﻿var _menu = _menu || {};
$.extend(true, _menu, {

    Menu: {
        error: function () {
            alert("Hubo un error no controlado");
        },
        init: function () {
            var countReference = 0;
            var menuNode = function (nombre, idPadre, url, id) {
                this.children = [];
                this.Nombre = nombre;
                this.IdPadre = idPadre;
                this.Id = id;
                this.Url = url;
            }

            menuNode.prototype = {
                add: function (child) {
                    this.children.push(child);
                },

                remove: function (child) {
                    var length = this.children.length;
                    for (var i = 0; i < length; i++) {
                        if (this.children[i] === child) {
                            this.children.splice(i, 1);
                            return;
                        }
                    }
                },

                getChild: function (i) {
                    return this.children[i];
                },

                hasChildren: function () {
                    return this.children.length > 0;
                }
            }

            var MainNode = {};
            var containerClone = $("<div></div>");

            $("#accordion")
                .accordion({
                    header: ".accordion-section",
                    collapsible: true,
                    heightStyle: 'fill',
                })
                .sortable({
                    axis: "y",
                    items: '.container', //selector which wraps the header and content
                    handle: ".accordion-order",
                    stop: function (event, ui) {
                        // IE doesn't register the blur when sorting
                        // so trigger focusout handlers to remove .ui-state-focus
                        ui.item.children(".accordion-order").triggerHandler("focusout");
                    }
                });
            $("#addPadre").click(function (e) {
                debugger;
                $("#nodos").show();
                var nombre = $(this).closest(".padre").find(".Name")[0];
                var url = $(this).closest(".padre").find(".Url")[0];
                MainNode = new menuNode($(nombre).val(), 0, $(url).val(), 0);
            });
            $(containerClone).html($("#accordion").html());

            $(document).on("click", ".addHijo", function (e) {
                debugger;
                var parents = $($(this).parents("li")).length;
                var actualLi = $(this).closest("li");
                var divToDraw = $(this).closest('.reference');
                var ulId = $("#myMenu").find("div[class='row reference']").length;
                var ul = $("<ul></ul>");
                var li = $("<li data-id='" + (ulId + 1) + "'></li>");
      
                $(li).html("<div class='row reference'>" + $(divToDraw).html() + "</div>")
                $(ul).append(li);
                $(actualLi).append(ul);

                //var nombre = $(this).closest('.menuContent').find(".Name")[0];
                //var url = $(this).closest('.menuContent').find(".Url")[0];

                //if (ulId == 1) {
                //    // si es el primer nodo se lo agrega 
                //    var Node = new menuNode($(nombre).val(), 0, $(url).val(), ulId);
                //    findPurpose(idPadre, MainNode, Node);

                //} else {
                //    var idPadre = ulId == 1 ? 0 : $($($(this).parents("li")).get(parents - 1)).data("id");
                //    var Node = new menuNode($(nombre).val(), idPadre, $(url).val(), ulId + 1);
                //    var res = findPurpose(idPadre, MainNode, Node);
                //}
                e.stopImmediatePropagation();
            });
            /////
            $(".addHijo").click(function (e) {
                debugger;
                var parents = $($(this).parents("li")).length;
                var actualLi = $(this).closest("li");
                var divToDraw = $(this).closest('.reference');
                var ulId = $("#myMenu").find("div[class='row reference']").length;
                var ul = $("<ul></ul>");
                var li = $("<li data-id='" + (ulId + 1) + "'></li>");

                $(li).html("<div class='row reference'>" + $(divToDraw).html() + "</div>")
                $(ul).append(li);
                $(actualLi).append(ul);

                //var nombre = $(this).closest('.menuContent').find(".Name")[0];
                //var url = $(this).closest('.menuContent').find(".Url")[0];
                //if (ulId == 1) {
                //    // si es el primer nodo se lo agrega 
                //    var Node = new menuNode($(nombre).val(), 0, $(url).val(), ulId);
                //    findPurpose(idPadre, MainNode, Node);

                //} else {
                //    var idPadre = ulId == 1 ? 0 : $($($(this).parents("li")).get(parents - 1)).data("id");
                //    var Node = new menuNode($(nombre).val(), idPadre, $(url).val(), ulId + 1);
                //    var res = findPurpose(idPadre, MainNode, Node);
                //}
                e.stopImmediatePropagation();
            });
            $(document).on("click", ".addHermano", function (e) {
                debugger;
                var parents = $($(this).parents("li")).length;
                var divD = $("<div></div>");
                var divToDraw = $(this).closest('.reference');
                var ulId = $("#myMenu").find("div[class='row reference']").length;//$(this).closest('.lista').children().length;
                var li = $("<li data-id='" + (ulId + 1) + "'></li>");

                $(li).html("<div class='row reference'>" + $(divToDraw).html() + "</div>");
                $(this).closest('ul').append(li);

                //var nombre = $(this).closest('.menuContent').find(".Name")[0];
                //var url = $(this).closest('.menuContent').find(".Url")[0];
                //if (ulId == 1) {
                //    // si es el primer nodo se lo agrega 
                //    var Node = new menuNode($(nombre).val(), 0, $(url).val(), ulId);
                //    findPurpose(idPadre, MainNode, Node);

                //} else {
                //    var idPadre = ulId == 1 ? 0 : $($($(this).parents("li")).get(parents - 1)).data("id");
                //    var Node = new menuNode($(nombre).val(), idPadre, $(url).val(), ulId + 1);
                //    var res = findPurpose(idPadre, MainNode, Node);
                //}

                e.stopImmediatePropagation();

            });
            $(".addHermano").click(function (e) {
                debugger;
                var parents = $($(this).parents("li")).length;
                var divD = $("<div></div>");
                var divToDraw = $(this).closest('.reference');
                var ulId = $("#myMenu").find("div[class='row reference']").length;//$(this).closest('.lista').children().length;
                var li = $("<li data-id='" + (ulId + 1) + "'></li>");

                $(li).html("<div class='row reference'>" + $(divToDraw).html() + "</div>");
                $(this).closest('ul').append(li);

                //var nombre = $(this).closest('.menuContent').find(".Name")[0];
                //var url = $(this).closest('.menuContent').find(".Url")[0];

                //if (ulId == 1) {
                //    // si es el primer nodo se lo agrega 
                //    var Node = new menuNode($(nombre).val(), 0, $(url).val(), ulId);
                //    findPurpose(idPadre, MainNode, Node);

                //} else {
                //    var idPadre = ulId == 1 ? 0 : $($($(this).parents("li")).get(parents - 1)).data("id");
                //    var Node = new menuNode($(nombre).val(), idPadre, $(url).val(), ulId +1);
                //    var res = findPurpose(idPadre, MainNode, Node);
                //}

                e.stopImmediatePropagation();
            });

            function findPurpose(id, object, NodeToAdd) {
                if (object.Id == id) {
                    object.children.push(NodeToAdd);
                    return object;
                } else {
                    $.grep(object.children, function (n, i) {
                        if (n.Id == id) {
                            n.children.push(NodeToAdd);
                            return n;
                        } else {
                            findPurpose(id, n, NodeToAdd);
                        }
                    });
                }
            };
        }
    }
});