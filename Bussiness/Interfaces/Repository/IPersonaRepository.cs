﻿using BussinessModel.Interfaces.Repository;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness.Interfaces.Repository
{
    public interface IPersonaRepository : ICommonRepository<Persona>
    { 
    }
}
