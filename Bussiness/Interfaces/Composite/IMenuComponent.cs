﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessModel.Interfaces.Composite
{
    public interface IMenuComponent
    {
        void Add(IMenuComponent c);
        void Remove(IMenuComponent c);
        void Display(int depth);
    }
}
